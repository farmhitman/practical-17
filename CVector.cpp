#include "CVector.h"
#include <corecrt_math.h>

CVector3D::CVector3D()
{
	this->x = 0.f;
	this->y = 0.f;
	this->z = 0.f;
}

CVector3D::CVector3D(float x, float y, float z)
{
	this->x = x;
	this->y = y;
	this->z = z;
}

float CVector3D::GetX()
{
	return this->x;
}

float CVector3D::GetY()
{
	return this->y;
}

float CVector3D::GetZ()
{
	return this->z;
}

float CVector3D::Size()
{
	float mSum = pow(x, 2) + pow(y, 2) + pow(z, 2);
	return sqrt(mSum);
}
