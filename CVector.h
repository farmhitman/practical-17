#pragma once
class CVector3D
{
private:
	float x;
	float y;
	float z;
public:
	CVector3D();
	CVector3D(float x, float y, float z);
	float GetX();
	float GetY();
	float GetZ();
	float Size();
};

