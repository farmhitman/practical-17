#include <iostream>
#include "CVector.h"

int main()
{
    CVector3D mVector(1.1, 2.2, 3.3);
    printf("Vector = %.2f, %.2f, %.2f\n", mVector.GetX(), mVector.GetY(), mVector.GetZ());
    printf("Module = %.3f\n", mVector.Size());
}
